using System;

namespace sotdl
{
    static class RollFunctions
    {
        static private int roll(int min, int max, int times = 1)
        {
            var randomGenerator = new Random();
            int result = 0;
            for (int i = 0; i < times; ++i)
            {
                result += randomGenerator.Next(min, max + 1);
            }

            return result;
        }

        public static int Rolld20(int diceCount = 1)
        {
            return roll(1, 20, diceCount);
        }

        public static int Rolld6(int diceCount = 1)
        {
            return roll(1, 6, diceCount);
        }
    }

}