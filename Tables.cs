using static sotdl.RollFunctions;

namespace sotdl
{
    static class Form
    {
        public const string Tablet = "Tablet";
        public const string Scroll = "Scroll";
        public const string Folio = "Folio";
        public const string Book = "Book";
        public const string Tome = "Tome";
        public const string Codex = "Codex";
        public static string[] Forms = new string[16] {
            // 3 - 4
            Tablet,
            Tablet,
            // 5 - 8
            Scroll,
            Scroll,
            Scroll,
            Scroll,
            // 9 - 12
            Folio,
            Folio,
            Folio,
            Folio,
            // 13 -15
            Book,
            Book,
            Book,
            // 16 - 17
            Tome,
            Tome,
            // 18
            Codex
        };

        public static string NewForm()
        {
            return Forms[Rolld6(3) - 3];
        }

        public static int GetPageCountForForm(string form)
        {
            switch (form)
            {
                case Folio:
                    return Rolld20() + 5;
                case Book:
                    return Rolld20(5);
                case Tome:
                    return Rolld20(10);
                case Codex:
                    return Rolld20(25);
                default:
                    return 1;
            }
        }
    }

    static class Material
    {
        public const string ClayOrWax = "Clay or wax";
        public const string Wood = "Wood";
        public const string Stone = "Stone";
        public const string Scale = "Scale of a large reptile";
        public const string Chitin = "Chitin";
        public const string Bone = "Bone";
        public const string Papyrus = "Papyrus";
        public const string Parchment = "Parchment";
        public const string Vellum = "Vellum";
        public const string Paper = "Paper";
        public const string Skin = "Snakeskin, scales or living skin";
        public const string HairTissueNails = "Hair, tissue, nails";
        public const string Onionskin = "Onionskin";
        public const string MetalPlates = "Metal plates";
        public const string Glass = "Glass";

        public static string[] TabletMaterials = new string[6] {
            ClayOrWax,
            Wood,
            Stone,
            Scale,
            Chitin,
            Bone,
        };

        public static string[] ScrollMaterials = new string[6] {
            Papyrus,
            Parchment,
            Vellum,
            Paper,
            Paper,
            Skin,
        };

        public static string[] PageMaterials = new string[16] {
            // 3
            HairTissueNails,
            // 4 - 5
            Parchment,
            Parchment,
            // 6 - 8
            Vellum,
            Vellum,
            Vellum,
            // 9 - 12
            Paper,
            Paper,
            Paper,
            Paper,
            // 13 - 15
            Onionskin,
            Onionskin,
            Onionskin,
            // 16 - 17
            MetalPlates,
            MetalPlates,
            // 18
            Glass,
        };

        public static string NewMaterial(string form)
        {
            switch (form)
            {
                case Form.Tablet:
                    return TabletMaterials[Rolld6() - 1];
                case Form.Scroll:
                    return ScrollMaterials[Rolld6() - 1];
                default:
                    return PageMaterials[Rolld6(3) - 3];
            }
        }
    }

    static class Condition
    {
        public const string Damaged = "Considerably damaged with most pages eaten by worms, burned, or missing.Water damage might make most of it illegible, too.";
        public const string LightUse = "Some light use";
        public const string Worn = "Worn with several bent or torn pages, scratches, tears, or stains on the cover";
        public const string PoorShape = "Poor shape with loose pages, extensive staining, and possible burns";
        public const string New = "New or appears new";

        public static string[] Conditions = new string[16] {
            // 3 - 5
            Damaged,
            Damaged,
            Damaged,
            // 6 - 8
            LightUse,
            LightUse,
            LightUse,
            // 9 - 12
            Worn,
            Worn,
            Worn,
            Worn,
            // 13 - 15
            PoorShape,
            PoorShape,
            PoorShape,
            // 16 - 18
            New,
            New,
            New,
        };

        public static string NewCondition()
        {
            return Conditions[Rolld6(3) - 3];
        }
    }
}