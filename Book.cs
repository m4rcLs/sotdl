using System;
using static sotdl.Form;
using static sotdl.Material;
using static sotdl.Condition;

namespace sotdl
{
    class Literature
    {
        public string Form { get; }
        public string Material { get; }
        public int Pages { get; }
        public string Condition { get; }

        public Literature()
        {
            Form = NewForm();
            Material = NewMaterial(Form);
            Pages = GetPageCountForForm(Form);
            Condition = NewCondition();
        }

        public override string ToString()
        {
            var output = $"A {Form}";
            if (Pages > 1) output += $" with {Pages} pages";

            output += $" made out of { Material.ToLower()}";
            output += $"Condition is {Condition.ToLower()}";
            return output;
        }

        //  cover
        //  condition
        //  pages
        //  language
        //  authors
        //  ink
        //  content
        //  illustrations
        //  illustration_quality
    }
}